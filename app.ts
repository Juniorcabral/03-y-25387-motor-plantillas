import express from 'express';
import path from 'path';
import hbs from 'hbs';
const app = express();
const publicRoutes = require('./routes/public.js');
app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

hbs.registerPartials(path.join(__dirname, 'views', 'partials'));

app.use('/', publicRoutes);

app.listen(3000, () => {
    console.log("Server is running on port http://localhost:3000/");
});