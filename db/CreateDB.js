const sqlite3 = require('sqlite3').verbose();
const fs = require('fs').promises;
const path = require('path');

const createTablesPath = path.join(__dirname, 'crebas.sql');
const insertDataPath = path.join(__dirname, 'inserts.sql');

const db = new sqlite3.Database('./Inge3.db', (err) => {
    if (err) {
        console.error(err.message);
    }
    console.log('Conectado a la base de datos Inge3.');
});

function executeSQLFile(filePath) {
    return fs.readFile(filePath, 'utf8')
        .then(data => {
            return new Promise((resolve, reject) => {
                db.exec(data, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log(`Archivo SQL ${filePath} ejecutado correctamente.`);
                        resolve();
                    }
                });
            });
        });
}

// Ejecutar las operaciones de archivo en serie
executeSQLFile(createTablesPath)
    .then(() => executeSQLFile(insertDataPath))
    .then(() => {
        db.close((err) => {
            if (err) {
                console.error(err.message);
            }
            console.log('Conexión a la base de datos cerrada.');
        });
    })
    .catch(err => console.error(err));
