const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const dbPath = path.resolve(__dirname, 'Inge3.db');
const db = new sqlite3.Database(dbPath);

// TODO Para verificar que la conexión con la base de datos sea exitosa
// db.serialize(() => {
//     db.run('SELECT 1', [], (err) => {
//         if (err) {
//             console.error('Error al conectar con la base de datos:', err);
//         } else {
//             console.log('Conexión exitosa con la base de datos.');
//         }
//     });
// });

// Función para obtener todos los integrantes
function getIntegrantes() {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM integrantes WHERE activo = 1 order by orden', [], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}
// Función para obtener un integrante por matricula
function getIntegranteByMatricula(matricula) {
    return new Promise((resolve, reject) => {
        db.get('SELECT * FROM integrantes WHERE activo = 1 AND matricula = ?', [matricula], (err, row) => {
            if (err) {
                reject(err);
            } else {
                resolve(row);
            }
        });
    });
}

// Función para obtener todos los medios por matricula
function getMediaByMatricula(matricula) {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM media WHERE activo = 1 AND matricula = ?', [matricula], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}
// Función para obtener los datos de home
function getHome() {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM home', [], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}

// Función para obtener los datos de informacion
function getInformacion() {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM informacion', [], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}

// Función para obtener los datos de tipoMedia
function getTipoMedia() {
    return new Promise((resolve, reject) => {
        db.all('SELECT * FROM tipo_media WHERE activo = 1 order by orden', [], (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
}

// Exportar las funciones
module.exports = {
    getIntegrantes,
    getMediaByMatricula,
    getHome,
    getInformacion,
    getTipoMedia,
    getIntegranteByMatricula
};