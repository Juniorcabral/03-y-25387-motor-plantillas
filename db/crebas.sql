CREATE TABLE IF NOT EXISTS "integrantes" (
	"id" INTEGER,
	"matricula" TEXT NOT NULL,
	"nombre" TEXT NOT NULL,
	"apellido" TEXT NOT NULL,
	"descripcion" TEXT,
	"activo" INTEGER,
	"orden" INTEGER	
);

CREATE TABLE IF NOT EXISTS "tipo_media" (
	"id" INTEGER NOT NULL UNIQUE,
	"nombre" TEXT NOT NULL,
	"activo" INTEGER,
	"orden" INTEGER,
	PRIMARY KEY("id")	
);

CREATE TABLE IF NOT EXISTS "media" (
	"id" INTEGER NOT NULL UNIQUE,
	"src" TEXT,
	"url" TEXT,
	"titulo" TEXT,
	"alt" TEXT,
	"activo" INTEGER,
	"orden" INTEGER,
	"matricula" TEXT NOT NULL,
	"tipo_media_id" INTEGER NOT NULL,
	PRIMARY KEY("id"),
	FOREIGN KEY ("tipo_media_id") REFERENCES "tipo_media"("id")
	ON UPDATE NO ACTION ON DELETE NO ACTION
    FOREIGN KEY ("matricula") REFERENCES "integrantes"("matricula")
    ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE IF NOT EXISTS "Home" (
	"id" INTEGER NOT NULL UNIQUE,
	"nombre" TEXT,
	"titulo" TEXT,
	"src" TEXT,
	"alt" TEXT,
	"descripcion" TEXT,
	PRIMARY KEY("id")	
);

CREATE TABLE IF NOT EXISTS "informacion" (
	"id" INTEGER NOT NULL UNIQUE,
	"href" TEXT,
	"nombre" TEXT,
	PRIMARY KEY("id")	
);
